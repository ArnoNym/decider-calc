package an.decider.calc;

import an.decider.input.testImplementation.Demander;
import an.decider.input.testImplementation.Offer;
import an.decider.input.testImplementation.Opinion;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

class MemberTest {

    @Test
    void joinMaxUtilityGroup() {
        Group g1 = new Group(new Offer(1, "o1", false, 1, 1, null));
        Group g2 = new Group(new Offer(2, "o2", false, 1, 1, null));
        Group g3 = new Group(new Offer(3, "o3", false, 1, 1, null));
        Set<Group> groups = new HashSet<>();
        groups.add(g1);
        groups.add(g2);
        groups.add(g3);
        Demander d1 = new Demander(1, "d1", false);
        d1.opinions.add(new Opinion(1, 1, 5));
        d1.opinions.add(new Opinion(1, 2, 6));
        d1.opinions.add(new Opinion(1, 3, 5));
        Member m1 = new Member(d1);
        m1.joinBestAvailableGroup(groups);
        Assert.assertFalse(g1.contains(m1));
        Assert.assertTrue(g2.contains(m1));
        Assert.assertFalse(g3.contains(m1));
    }

    @Test
    void switchGroupsIfAddUtility() {
    }

    @Test
    void switchGroupsIfEqualUtility() {
    }

    @Test
    void calcSwitchGroupsUtility() {
    }

    @Test
    void equalGroup() {
    }
}