package an.decider.calc;

import an.decider.input.testImplementation.Decision;
import an.decider.input.testImplementation.Demander;
import an.decider.input.testImplementation.Offer;
import an.decider.input.testImplementation.Opinion;

import org.junit.jupiter.api.Test;

class DeciderTest {

    @Test
    void createSortedOfferCombinations_1() {
        Decision decision = new Decision("decision-name", 1, null);

        // Offers =====================================================================================================

        Offer o1 = new Offer(1, "o1", false, 1, 1, null);
        decision.offers.add(o1);

        Offer o2 = new Offer(2,"o2", false, 1, 1, null);
        decision.offers.add(o2);

        // Demanders =================================================================================

        Demander d1 = new Demander(1, "d1", false);
        d1.hasToBeIncluded = true;
        d1.opinions.add(new Opinion(1, 1, 1));
        d1.opinions.add(new Opinion(1, 2, 8));
        decision.demanders.add(d1);

        decision.validate();

        Demander d2 = new Demander(2, "d2", false);
        d2.hasToBeIncluded = true;
        d2.opinions.add(new Opinion(2, 1, 1));
        d2.opinions.add(new Opinion(2, 2, 5));
        decision.demanders.add(d2);

        decision.validate();

        Demander d3 = new Demander(3, "d3", false);
        d3.hasToBeIncluded = true;
        d3.opinions.add(new Opinion(3, 1, 1));
        d3.opinions.add(new Opinion(3, 2, 5));
        decision.demanders.add(d3);

        decision.validate();

        Demander d4 = new Demander(4, "d4", false);
        d4.hasToBeIncluded = true;
        d4.opinions.add(new Opinion(4, 1, 7));
        d4.opinions.add(new Opinion(4, 2, 2));
        decision.demanders.add(d4);

        decision.validate();

        Decider.createChoiceList(decision);
    }

    @Test
    public void createSortedOfferCombinations_2() {
        Decision decision = new Decision("decision-name", 1, null);

        // Offers =====================================================================================================

        Offer o1 = new Offer(1, "o1", false, 1, 1, null);
        decision.offers.add(o1);

        Offer o2 = new Offer(2,"o2", false, 1, 1, null);
        decision.offers.add(o2);

        // Demanders =================================================================================

        Demander d1 = new Demander(1, "d1", false);
        d1.hasToBeIncluded = true;
        d1.opinions.add(new Opinion(1, 1, 1));
        d1.opinions.add(new Opinion(1, 2, 8));
        decision.demanders.add(d1);

        decision.validate();

        Demander d2 = new Demander(2, "d2", false);
        d2.hasToBeIncluded = true;
        d2.opinions.add(new Opinion(2, 1, 5));
        d2.opinions.add(new Opinion(2, 2, 5));
        decision.demanders.add(d2);

        decision.validate();

        Demander d3 = new Demander(3, "d3", false);
        d3.hasToBeIncluded = true;
        d3.opinions.add(new Opinion(3, 1, 5));
        d3.opinions.add(new Opinion(3, 2, 5));
        decision.demanders.add(d3);

        decision.validate();

        Demander d4 = new Demander(4, "d4", false);
        d4.hasToBeIncluded = true;
        d4.opinions.add(new Opinion(4, 1, 7));
        d4.opinions.add(new Opinion(4, 2, 2));
        decision.demanders.add(d4);

        decision.validate();

        Decider.createChoiceList(decision);
    }

    @Test
    public void createSortedOfferCombinations_3() {
        Decision decision = new Decision("decision-name", 1, null);

        // Offers =====================================================================================================

        Offer o1 = new Offer(1, "o1", false, 1, 1, null);
        decision.offers.add(o1);

        Offer o2 = new Offer(2,"o2", false, 1, 1, null);
        decision.offers.add(o2);

        Offer o3 = new Offer(3, "o3", false, 1, 1, null);
        decision.offers.add(o3);

        Offer o4 = new Offer(4, "o4", false, 1, 1, null);
        decision.offers.add(o4);

        // Demanders =================================================================================

        Demander d1 = new Demander(1, "d1", false);
        d1.hasToBeIncluded = true;
        d1.opinions.add(new Opinion(1, 1, 5));
        d1.opinions.add(new Opinion(1, 2, 5));
        d1.opinions.add(new Opinion(1, 3, 5));
        d1.opinions.add(new Opinion(1, 4, 5));
        decision.demanders.add(d1);

        decision.validate();

        Demander d2 = new Demander(2, "d2", false);
        d2.hasToBeIncluded = true;
        d2.opinions.add(new Opinion(2, 1, 5));
        d2.opinions.add(new Opinion(2, 2, 5));
        d2.opinions.add(new Opinion(2, 3, 5));
        d2.opinions.add(new Opinion(2, 4, 5));
        decision.demanders.add(d2);

        decision.validate();

        Demander d3 = new Demander(3, "d3", false);
        d3.hasToBeIncluded = true;
        d3.opinions.add(new Opinion(3, 1, 5));
        d3.opinions.add(new Opinion(3, 2, 5));
        d3.opinions.add(new Opinion(3, 3, 5));
        d3.opinions.add(new Opinion(3, 4, 5));
        decision.demanders.add(d3);

        decision.validate();

        Demander d4 = new Demander(4, "d4", false);
        d4.hasToBeIncluded = true;
        d4.opinions.add(new Opinion(4, 1, 5));
        d4.opinions.add(new Opinion(4, 2, 5));
        d4.opinions.add(new Opinion(4, 3, 5));
        d4.opinions.add(new Opinion(4, 4, 5));
        decision.demanders.add(d4);

        decision.validate();

        Demander d5 = new Demander(5, "d5", false);
        d5.hasToBeIncluded = true;
        d5.opinions.add(new Opinion(5, 1, 5));
        d5.opinions.add(new Opinion(5, 2, 5));
        d5.opinions.add(new Opinion(5, 3, 5));
        d5.opinions.add(new Opinion(5, 4, 5));
        decision.demanders.add(d5);

        decision.validate();

        Demander d6 = new Demander(6, "d6", false);
        d6.hasToBeIncluded = true;
        d6.opinions.add(new Opinion(6, 1, 5));
        d6.opinions.add(new Opinion(6, 2, 5));
        d6.opinions.add(new Opinion(6, 3, 5));
        d6.opinions.add(new Opinion(6, 4, 5));
        decision.demanders.add(d6);

        decision.validate();

        Demander d7 = new Demander(7, "d7", false);
        d7.hasToBeIncluded = true;
        d7.opinions.add(new Opinion(7, 1, 5));
        d7.opinions.add(new Opinion(7, 2, 5));
        d7.opinions.add(new Opinion(7, 3, 5));
        d7.opinions.add(new Opinion(7, 4, 5));
        decision.demanders.add(d7);

        decision.validate();

        Decider.createChoiceList(decision);
    }

    /*
    @Test
    public void bestellungen() {
        Manager manager = new Manager();
        manager.setMinGroupCount(2);
        manager.setMinGroupCount(null);

        // Offers =================================================================================

        Offer italPizza = new Offer("italPizza", 1, 3, null);
        manager.addOffer(italPizza);

        Offer amerikaPizza = new Offer("amerikaPizza", 1, 3, null);
        manager.addOffer(amerikaPizza);

        Offer fanzy = new Offer("fanzy", 1, 3, null);
        manager.addOffer(fanzy);

        Offer anahong = new Offer("anahong", 1, 3, null);
        manager.addOffer(anahong);

        // Demanders =================================================================================

        Demander l = new Demander("L");
        l.setInsistsOnGroup(true);
        l.putOpinion(italPizza, 5);
        l.putOpinion(amerikaPizza, 8);
        l.putOpinion(fanzy, 4);
        l.putOpinion(anahong, 10);
        manager.addDemander(l);

        Demander tim = new Demander("Tim");
        l.setInsistsOnGroup(true);
        tim.putOpinion(italPizza, 7);
        tim.putOpinion(amerikaPizza, 4);
        tim.putOpinion(fanzy, 3);
        tim.putOpinion(anahong, 4);
        manager.addDemander(tim);

        Demander koenig = new Demander("Koenig");
        l.setInsistsOnGroup(true);
        koenig.putOpinion(italPizza, 7);
        koenig.putOpinion(amerikaPizza, 8);
        koenig.putOpinion(fanzy, 8);
        koenig.putOpinion(anahong, 7);
        manager.addDemander(koenig);

        Demander hendrik = new Demander("Hendrik");
        l.setInsistsOnGroup(true);
        hendrik.putOpinion(italPizza, 8);
        hendrik.putOpinion(amerikaPizza, -1);
        hendrik.putOpinion(fanzy, -1);
        hendrik.putOpinion(anahong, -1);
        manager.addDemander(hendrik);

        Demander zora = new Demander("Zora");
        l.setInsistsOnGroup(true);
        zora.putOpinion(italPizza, 6);
        zora.putOpinion(amerikaPizza, 6);
        zora.putOpinion(fanzy, 4);
        zora.putOpinion(anahong, 4);
        manager.addDemander(zora);

        Demander annCathrin = new Demander("Ann-Cathrin");
        l.setInsistsOnGroup(true);
        annCathrin.putOpinion(italPizza, 8);
        annCathrin.putOpinion(amerikaPizza, 5);
        annCathrin.putOpinion(fanzy, 5);
        annCathrin.putOpinion(anahong, 5);
        manager.addDemander(annCathrin);

        Demander rico = new Demander("Rico");
        rico.putOpinion(italPizza, 10);
        rico.putOpinion(amerikaPizza, 1);
        rico.putOpinion(fanzy, 1);
        rico.putOpinion(anahong, 1);
        manager.addDemander(rico);

        List<Choice> choices = manager.createChoices();
        for (Choice choice : choices) {
            System.out.println(choice.toString() + "\n");
        }
    }
    */
}