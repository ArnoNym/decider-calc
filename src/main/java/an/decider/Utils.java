package an.decider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Utils {
    /*
    public static <O> O highestValueObject(Collection<O> collection, Function<O, Integer> function, int startValue) {
        int maxValue = startValue;
        O maxValueO = null;
        for (O o : collection) {
            int value = function.apply(o);
            if (value > maxValue) {
                maxValue = value;
                maxValueO = o;
            }
        }
        return maxValueO;
    }

    public static <O> O lowestValueObject(Collection<O> collection, Function<O, Double> function, double startValue) {
        double minValue = startValue;
        O minValueO = null;
        for (O o : collection) {
            double value = function.apply(o);
            if (value < minValue) {
                minValue = value;
                minValueO = o;
            }
        }
        return minValueO;
    }

    // Power Set ==================================================================================
    public static <O> Set<List<O>> createPowerSet(ArrayList<O> objects, int minGroupSize,
                                                  @Nullable Integer maxGroupSize) {
        if (objects.size() < 1) {
            throw new IllegalArgumentException();
        }
        if (minGroupSize < 1) {
            throw new IllegalArgumentException();
        }
        if (maxGroupSize != null) {
            if (maxGroupSize < 1) {
                throw new IllegalArgumentException();
            }
            if (minGroupSize > maxGroupSize) {
                throw new IllegalArgumentException();
            }
        }
        Set<List<O>> powerSet = new HashSet<>((int) Math.pow(2, objects.size()) - 1);
        createPowerSet(powerSet, () -> new ArrayList<>(), objects, minGroupSize, maxGroupSize, null, null);
        return powerSet;
    }

    public static <COLL extends Collection<O>, O> void createPowerSet(
            Collection<COLL> powerColl,
            Supplier<? extends COLL> collSupplier,
            ArrayList<O> objects,
            int minGroupSize,
            @Nullable Integer maxGroupSize,
            @Nullable Predicate<COLL> isInvalidSetAndSubsets,
            @Nullable Predicate<COLL> isInvalidSet) {

        int size = objects.size();
        if (size < minGroupSize) {
            return;
        }
        COLL newColl = collSupplier.get();
        newColl.addAll(objects);
        if (isInvalidSetAndSubsets != null && isInvalidSetAndSubsets.test(newColl)) {
            return;
        }
        if (maxGroupSize == null || size <= maxGroupSize) {
            if (isInvalidSet == null || !isInvalidSet.test(newColl)) {
                if (!powerColl.add(newColl)) {
                    // todo testen. Das sollte Performance verbessern    return;
                }
            }
            if (size == minGroupSize) {
                return;
            }
        }
        for (int i = 0; i < size; i++) {
            ArrayList<O> objectsMinusOne = new ArrayList<>(objects);
            objectsMinusOne.remove(i);
            createPowerSet(powerColl, collSupplier, objectsMinusOne, minGroupSize, maxGroupSize,
                    isInvalidSetAndSubsets, isInvalidSet);
        }
    }
    */
}
