package an.decider.output;

import an.decider.calc.Group;
import an.decider.input.Demander;
import an.decider.input.Offer;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class Pack {
    @NotNull
    private final Offer offer;
    @NotNull
    private final List<Demander> demanderList;

    public Pack(@NotNull Group group) {
        this.offer = group.getOffer();
        this.demanderList = new LinkedList<>(group.getDemanders());
    }

    public void validate() {
        offer.validate();
        if (demanderList.isEmpty()) throw new IllegalStateException();
        for (Demander demander : demanderList) {
            demander.validate();
        }
    }

    public float calcUtility() {
        float utility = 0;
        for (Demander demander : demanderList) {
            utility += demander.getUtility(offer);
        }
        return utility;
    }

    @NotNull
    public Offer getOffer() {
        return offer;
    }

    public List<Demander> getDemanderList() {
        return Collections.unmodifiableList(demanderList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(offer, demanderList);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Pack)) return false;
        Pack other = (Pack) obj;
        return offer.equals(other.offer) && demanderList.equals(other.demanderList);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(offer.name()).append(": ");
        for (Demander demander : demanderList) {
            sb.append(demander.name())
                    .append("(")
                    .append(demander.getRank(offer))
                    .append(",")
                    .append(demander.getUtility(offer))
                    .append(")")
                    .append(", ");
        }
        return sb.toString();
    }
}
