package an.decider.output;

import java.util.*;

import an.decider.Const;
import an.decider.calc.Group;
import an.decider.calc.Member;
import an.decider.input.Demander;
import an.decider.input.Offer;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Choice {
    @NotNull
    private final List<Pack> packList;

    private Choice(@NotNull Set<Group> groups) {
        if (groups.isEmpty()) {
            throw new IllegalArgumentException();
        }
        packList = new LinkedList<>();//todo in irgendeiner reihenfolge ordnen
        for (Group group : groups) {
            packList.add(new Pack(group));
        }
    }

    public void validate() {
        if (packList.isEmpty()) throw new IllegalStateException();
        for (Pack pack : packList) {
            pack.validate();
        }
    }

    @NotNull
    public List<Pack> getPackList() {
        return packList;
    }

    public double calcUtility() {
        double utility = 0;
        for (Pack pack : packList) {
            utility += pack.calcUtility();
        }
        return utility;
    }

    @NotNull
    public List<Demander> calcDemanderList() {
        Set<Demander> demanderSet = new HashSet<>();
        for (Pack pack : packList) {
            demanderSet.addAll(pack.getDemanderList());
        }
        return new LinkedList<>(demanderSet);
    }

    @NotNull
    public List<Offer> calcOfferList() {
        Set<Offer> offerSet = new HashSet<>();
        for (Pack pack : packList) {
            offerSet.add(pack.getOffer());
        }
        return new LinkedList<>(offerSet);
    }

    @Override
    public int hashCode() {
        return packList.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Choice)) return false;
        Choice other = (Choice) obj;
        return packList.equals(other.packList);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Utility: ").append(calcUtility());
        for (Pack pack : packList) {
            sb.append("\n-> ").append(pack.toString());
        }
        return sb.toString();
    }

    // ================================================================================================================
    // PREPARE GROUPS
    // ================================================================================================================

    @Nullable
    private static Set<Group> prepareGroups(@NotNull Collection<? extends Demander> demanders, List<Offer> offerList) {
        if (demanders.isEmpty()) throw new IllegalArgumentException();
        for (Demander demander : demanders) demander.validate();
        if (offerList.isEmpty()) throw new IllegalArgumentException();
        for (Offer offer : offerList) offer.validate();

        /* 1) Check if min- und max-group-size-constrains can be fulfilled (max ist just for performance at this point.
        Is also enforced by 4)) */
        int hasToBeIncludedDemanderCount = 0;
        for (Demander demander : demanders) {
            if (demander.hasToBeIncluded()) {
                hasToBeIncludedDemanderCount += 1;
            }
        }
        int minCount = 0;
        Integer maxCount = 0;
        for (Offer offer : offerList) {
            minCount += offer.minGroupSize();
            if (maxCount != null) {
                if (offer.maxGroupSize().isEmpty()) {
                    maxCount = null;
                } else {
                    maxCount += offer.maxGroupSize().get();
                }
            }
        }
        if (minCount > demanders.size() || (maxCount != null && maxCount < hasToBeIncludedDemanderCount)) {
            return null;
        }

        // 2) Create members
        Set<Member> members = new HashSet<>();
        for (Demander demander : demanders) {
            members.add(new Member(demander));
        }

        // 3) Create groups
        Set<Group> groups = new HashSet<>();
        for (Offer offer : offerList) {
            groups.add(new Group(offer));
        }

        /* 4) Ueberpruefe ob alle Spieler die in Gruppen sein muessen auch Gruppen finden (This is needed because
        members who need to be in a group can also forbid to join a group) */
        for (Member member : members) {
            if (member.insistsOnGroup()) {
                if (!member.joinBestAvailableGroup(groups)) {
                    return null;
                }
            }
        }

        // 5) maximize utility
        maximizeUtility(members, groups);

        /* 6) fulfill min-group-size-constraints: So umstaendlich, da durch das switchen schon als 'full' betitelte
        Gruppen wieder leer werden koennen */
        int fullGroupsCount = 0;
        while (fullGroupsCount < groups.size()) {
            for (Group group : groups) {
                if (group.fulfillsMinGroupSizeConstraint()) {
                    fullGroupsCount++;
                } else {
                    group.incrementBegging();
                    maximizeUtility(members, groups);
                    fullGroupsCount = 0;
                }
            }
        }

        return groups;
    }

    public static void maximizeUtility(@NotNull Set<Member> members, @NotNull Collection<Group> groups) {
        int fullRound = members.size();
        int countdown = fullRound;
        while (countdown != 0) {
            for (Member member : members) {
                /* Join noetig, da durch Begging Gruppen attraktiver werden, die vorher keine
                Mitglieder hatten */
                if (member.joinBestAvailableGroup(groups) | member.switchGroupsIfAddUtility(members)) {
                    countdown = fullRound - 1;
                } else {
                    countdown--;
                    if (countdown == 0) {
                        break;
                    }
                }
            }
        }
    }

    // ================================================================================================================
    // CREATE CHOICES
    // ================================================================================================================

    @Nullable
    @Contract(pure = true)
    public static Set<Choice> createEqualChoices(@NotNull Collection<? extends Demander> demanders,
                                                 @NotNull List<Offer> offerList) {
        Set<Group> groups = prepareGroups(demanders, offerList);
        if (groups == null) {
            return null;
        }

        Set<Choice> choices = new HashSet<>();

        choices.add(new Choice(groups));

        // todo auf den zweiten blick werden hier vermutlich nicht immer alle faelle abgedeckt
        Set<Member> members = new HashSet<>();
        for (Group group : groups) {
            members.addAll(group.getMembers());
        }
        for (Member m1 : members) {
            for (Member m2 : members) {
                if (!m1.equals(m2) && !m1.equalGroup(m2) && m1.switchGroupsIfEqualUtility(m2)) {
                    choices.add(new Choice(groups));
                }
            }
        }

        if (Const.DEVELOPER) {
            for (Choice choice : choices) {
                choice.validate();
            }
        }

        return choices;
    }
}
