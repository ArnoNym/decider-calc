package an.decider.input;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import an.decider.Const;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public interface Demander {
    Integer id();
    String name();
    Boolean hasToBeIncluded();
    Collection<? extends Opinion> opinions();

    @Contract(pure = true)
    default double getUtility(@NotNull Offer offer) {
        return getUtility(offer.id());
    }

    @Contract(pure = true)
    default double getUtility(@NotNull Integer offerId) {
        Double opinion = DemanderOpinions.offerIdOpinionMap(id(), opinions()).get(offerId);
        if (Const.DEVELOPER && opinion == null) {
            throw new IllegalStateException();
        }
        return opinion;
    }

    @Contract(pure = true)
    default double getRank(@NotNull Offer offer) {
        return getRank(offer.id());
    }

    @Contract(pure = true)
    default double getRank(@NotNull Integer offerId) {
        double opinion = getUtility(offerId);
        int rank = 1;
        for (double op : DemanderOpinions.offerIdOpinionMap(id(), opinions()).values()) {
            if (op > opinion) {
                rank++;
            }
        }
        return rank;
    }

    default void validate() {
        if (id() == null) throw new IllegalArgumentException();
        if (name() == null) throw new IllegalArgumentException();
        if (hasToBeIncluded() == null) throw new IllegalArgumentException();
        Set<Integer> offerIds = new HashSet<>();
        for (Opinion opinion : opinions()) {
            opinion.validate();
            Integer offerId = opinion.offerId();
            if (offerIds.contains(offerId)) throw new IllegalArgumentException("offerId="+offerId);
            offerIds.add(offerId);
        }
    }
}
