package an.decider.input;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

public interface Offer {
    Integer id();
    String name();
    Boolean hasToBeIncluded();
    Integer available();
    Integer minGroupSize();
    Optional<Integer> maxGroupSize();

    default void validate() {
        if (id() == null) throw new IllegalArgumentException();
        if (name() == null) throw new IllegalArgumentException();
        if (hasToBeIncluded() == null) throw new IllegalArgumentException();
        if (available() == null) throw new IllegalArgumentException();
        if (available() <= 0) throw new IllegalArgumentException();
        if (minGroupSize() == null) throw new IllegalArgumentException();
        if (minGroupSize() <= 0) throw new IllegalArgumentException();
        if (maxGroupSize().isPresent() && maxGroupSize().get() < minGroupSize()) throw new IllegalArgumentException();
    }
}
