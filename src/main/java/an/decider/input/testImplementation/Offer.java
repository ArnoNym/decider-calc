package an.decider.input.testImplementation;

import java.util.Optional;

public class Offer implements an.decider.input.Offer {
    public Integer id;
    public String name;
    public Boolean hasToBeIncluded;
    public Integer available;
    public Integer minGroupSize;
    public Integer maxGroupSize;

    public Offer(int id, String name, boolean hasToBeIncluded, int available, int minGroupSize, Integer maxGroupSize) {
        this.id = id;
        this.name = name;
        this.hasToBeIncluded = hasToBeIncluded;
        this.available = available;
        this.minGroupSize = minGroupSize;
        this.maxGroupSize = maxGroupSize;
    }

    @Override
    public Integer id() {
        return id;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public Boolean hasToBeIncluded() {
        return hasToBeIncluded;
    }

    @Override
    public Integer available() {
        return available;
    }

    @Override
    public Integer minGroupSize() {
        return minGroupSize;
    }

    @Override
    public Optional<Integer> maxGroupSize() {
        return Optional.ofNullable(maxGroupSize);
    }
}
