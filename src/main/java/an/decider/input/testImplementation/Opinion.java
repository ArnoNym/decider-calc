package an.decider.input.testImplementation;

public class Opinion implements an.decider.input.Opinion {
    public Integer demanderId;
    public Integer offerId;
    public Double value;

    public Opinion(Integer demanderId, Integer offerId, int value) {
        this(demanderId, offerId, (double) value);
    }

    public Opinion(Integer demanderId, Integer offerId, Double value) {
        this.demanderId = demanderId;
        this.offerId = offerId;
        this.value = value;
    }

    @Override
    public Integer demanderId() {
        return demanderId;
    }

    @Override
    public Integer offerId() {
        return offerId;
    }

    @Override
    public Double value() {
        return value;
    }
}
