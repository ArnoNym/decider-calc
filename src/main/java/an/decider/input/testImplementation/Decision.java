package an.decider.input.testImplementation;

import an.decider.input.Demander;
import an.decider.input.Offer;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

public class Decision implements an.decider.input.Decision {
    public String name;
    public Integer minGroupCount;
    public Integer maxGroupCount;
    public Collection<Offer> offers = new HashSet<>();
    public Collection<Demander> demanders = new HashSet<>();

    public Decision(String name, Integer minGroupCount, Integer maxGroupCount) {
        this.name = name;
        this.minGroupCount = minGroupCount;
        this.maxGroupCount = maxGroupCount;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public Integer minGroupCount() {
        return minGroupCount;
    }

    @Override
    public Optional<Integer> maxGroupCount() {
        return Optional.ofNullable(maxGroupCount);
    }

    @Override
    public Collection<? extends Offer> offers() {
        return offers;
    }

    @Override
    public Collection<? extends Demander> demanders() {
        return demanders;
    }
}
