package an.decider.input.testImplementation;

import an.decider.input.Opinion;

import java.util.Collection;
import java.util.HashSet;

public class Demander implements an.decider.input.Demander {
    public int id;
    public String name;
    public boolean hasToBeIncluded;
    public Collection<Opinion> opinions = new HashSet<>();

    public Demander(int id, String name, boolean hasToBeIncluded) {
        this.id = id;
        this.name = name;
        this.hasToBeIncluded = hasToBeIncluded;
    }

    @Override
    public Integer id() {
        return id;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public Boolean hasToBeIncluded() {
        return hasToBeIncluded;
    }

    @Override
    public Collection<? extends Opinion> opinions() {
        return opinions;
    }
}
