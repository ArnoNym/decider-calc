package an.decider.input;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public interface Opinion {
    Integer demanderId();
    Integer offerId();
    Double value();

    default void validate() {
        if (demanderId() == null) throw new IllegalArgumentException();
        if (offerId() == null) throw new IllegalArgumentException();
        if (value() == null) throw new IllegalArgumentException();
    }
}
