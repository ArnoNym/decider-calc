package an.decider.input;

import java.util.*;

public interface Decision {
    String name();
    Integer minGroupCount();
    Optional<Integer> maxGroupCount();
    Collection<? extends Demander> demanders();
    Collection<? extends Offer> offers();

    default void validate() {
        if (name() == null) throw new IllegalArgumentException();
        if (minGroupCount() == null) throw new IllegalArgumentException();
        if (minGroupCount() <= 0) throw new IllegalArgumentException();
        if (maxGroupCount().isPresent() && maxGroupCount().get() < minGroupCount()) throw new IllegalArgumentException();
        for (Demander demander : demanders()) {
            demander.validate();
        }
        for (Offer offer : offers()) {
            offer.validate();
        }
    }
}
