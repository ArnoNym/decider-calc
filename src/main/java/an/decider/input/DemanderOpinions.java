package an.decider.input;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class DemanderOpinions {
    private static Map<Integer, Map<Integer, Double>> demanderId_offerId_utility_map = new HashMap<>();

    @NotNull
    public static Map<Integer, Double> offerIdOpinionMap(@NotNull Integer demanderId,
                                                         @NotNull Collection<? extends Opinion> opinions) {
        if (opinions.isEmpty()) {
            return new HashMap<>();
        }

        Map<Integer, Double> offerId_utility_map = demanderId_offerId_utility_map.get(demanderId);
        if (offerId_utility_map == null) {
            offerId_utility_map = createMap(opinions);
        }
        return offerId_utility_map;
    }

    @NotNull
    private static Map<Integer, Double> createMap(@NotNull Collection<? extends Opinion> opinions) {
        Integer demanderId = opinions.iterator().next().demanderId();

        Map<Integer, Double> offerId_utility_map = new HashMap<>();
        demanderId_offerId_utility_map.put(demanderId, offerId_utility_map);

        for (Opinion opinion : opinions) {
            Integer offerId = opinion.offerId();
            Double utility = opinion.value();
            offerId_utility_map.put(offerId, utility);
        }

        return offerId_utility_map;
    }
}
