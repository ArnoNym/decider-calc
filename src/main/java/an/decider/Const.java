package an.decider;

public class Const {
    public static final boolean DEVELOPER = true;
    public static final int MIN_UTILITY = 0;
    public static final int MAX_UTILITY = 10;
    public static final int DEFAULT_UTILITY = 5;
    public static final int NO_WAY_UTILITY = -1; // Has to be lower than min utility
    public static final int UTILITY_ORDER_DECIMALS = 100;
}
