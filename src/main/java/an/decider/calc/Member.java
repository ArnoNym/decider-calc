package an.decider.calc;

import java.util.*;

import an.decider.input.Demander;
import an.decider.Const;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Member {
    private final Demander demander;
    private Group group;

    public Member(@NotNull Demander demander) {
        this.demander = demander;
    }

    @Contract(pure = true)
    public boolean insistsOnGroup() {
        return demander.hasToBeIncluded();
    }

    // Join Groups ================================================================================

    public boolean joinBestAvailableGroup(@NotNull Collection<Group> groups) {
        if (groups.isEmpty()) {
            return false;
        }

        Set<Group> joinGroups = new HashSet<>(groups);

        double maxUtility = -Double.MAX_VALUE;
        Group maxUtilityGroup = null;
        for (Group group : joinGroups) {
            double utility = group.getUtility(demander);
            if (utility > maxUtility) {
                maxUtility = utility;
                maxUtilityGroup = group;
            }
        }

        if (maxUtility == Const.NO_WAY_UTILITY) { // When this is the best option there is no group the member can join!
            return false;
        }
        if (maxUtilityGroup == null) {
            return false;
        }
        if (maxUtilityGroup.contains(this)) {
            return false;
        }

        if (joinGroup(maxUtilityGroup)) {
            return true;
        }

        joinGroups.remove(maxUtilityGroup);
        return joinBestAvailableGroup(joinGroups);
    }

    private boolean joinGroup(@Nullable Group group) {
        if (group != null) {
            if (!group.add(this)) {
                return false;
            }
        }
        if (this.group != null) {
            this.group.remove(this);
        }
        this.group = group;
        return true;
    }

    // Switch Groups ==================================================================================================

    public boolean switchGroupsIfAddUtility(@NotNull Collection<Member> members) {
        double maxAddUtility = 0;
        Member switchWithMember = null;
        for (Member member : members) {
            double addUtility = calcSwitchGroupsUtility(member);
            if (addUtility > maxAddUtility) {
                maxAddUtility = addUtility;
                switchWithMember = member;
            }
        }
        if (maxAddUtility == 0) {
            return false;
        }
        switchGroups(switchWithMember, false);
        return true;
    }

    public boolean switchGroupsIfEqualUtility(@NotNull Member other) {
        double addUtility = this.calcSwitchGroupsUtility(other);
        if (addUtility > 0) {
            throw new IllegalStateException();
        }
        if (addUtility == 0) {
            switchGroups(other, true);
            return true;
        }
        return false;
    }

    public double calcSwitchGroupsUtility(@NotNull Member other) {
        final double otherCurrentUtility = other.getUtility();
        final double otherPossibleUtility;
        if (this.group == null) {
            otherPossibleUtility = 0;
        } else {
            otherPossibleUtility = this.group.getUtility(other.demander);
        }

        final double thisCurrentUtility = getUtility();
        final double thisPossibleUtility;
        if (other.group == null) {
            thisPossibleUtility = 0;
        } else {
            thisPossibleUtility = other.group.getUtility(this.demander);
        }

        return otherPossibleUtility - otherCurrentUtility + thisPossibleUtility - thisCurrentUtility;
    }

    private void switchGroups(@NotNull Member other, boolean force) {
        if (this.group != null) {
            if (!this.group.remove(this)) throw new IllegalStateException();
            if (!this.group.add(other, force)) throw new IllegalStateException();
        }
        if (other.group != null) {
            if (!other.group.remove(other)) throw new IllegalStateException();
            if (!other.group.add(this, force)) throw new IllegalStateException();
        }
        Group thisOldGroup = this.group;
        this.group = other.group;
        other.group = thisOldGroup;
    }

    // Utility ========================================================================================================

    public double getUtility() {
        if (group == null) {
            return 0;
        }
        return group.getUtility(demander);
    }

    // ================================================================================================================

    public Demander getDemander() {
        return demander;
    }

    public boolean equalGroup(Member member) {
        if (member == null) {
            return false;
        }
        if (this.group == null) {
            return false;
        }
        if (member.group == null) {
            return false;
        }
        return this.group.equals(member.group);
    }

    // Print ==========================================================================================================

    public void validate(boolean validateGroup) {
        demander.validate();
        if (validateGroup && group != null) group.validate();
    }

    public void validate() {
        validate(true);
    }

    @NotNull
    @Override
    public String toString() {
        return demander.toString();
    }
}
