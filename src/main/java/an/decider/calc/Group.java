package an.decider.calc;

import java.util.*;

import an.decider.Const;
import an.decider.input.Demander;
import an.decider.input.Offer;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public class Group {
    @NotNull
    private final Offer offer;
    @NotNull
    private final Set<Member> members = new HashSet<>();
    private int begging = 0;

    public Group(@NotNull Offer offer) {
        this.offer = offer;
    }

    // Utility ========================================================================================================

    @Contract(pure = true)
    public double getUtility(@NotNull Demander demander) {
        double utility = demander.getUtility(offer);
        if (utility == Const.NO_WAY_UTILITY) {
            return utility;
        }
        return utility + begging;
    }

    // Members ========================================================================================================

    @Contract(pure = true, value = " -> new")
    public Set<Member> getMembers() {
        return new HashSet<>(members);
    }

    @Contract(pure = true, value = " -> new")
    public Set<Demander> getDemanders() {
        Set<Demander> currentDemanders = new HashSet<>();
        for (Member member : members) {
            currentDemanders.add(member.getDemander());
        }
        return currentDemanders;
    }

    public boolean add(Member member) {
        return add(member, false);
    }

    public boolean add(Member member, boolean ignoreRejected) {
        if (member == null) {
            throw new IllegalArgumentException();
        }
        int size = size();
        if (begging > 0) {
            if (size == offer.minGroupSize()) {
                return false;
            }
            if (Const.DEVELOPER && size > offer.minGroupSize()) {
                throw new IllegalStateException();
            }
        }
        Optional<Integer> maxGroupSize = offer.maxGroupSize();
        if (maxGroupSize.isPresent() && maxGroupSize.get() <= size) {
            return false;
        }
        return members.add(member);
    }

    public boolean remove(Member member) {
        return members.remove(member);
    }

    public boolean contains(Member member) {
        return members.contains(member);
    }

    public boolean isEmpty() {
        return members.isEmpty();
    }

    public int size() {
        return members.size();
    }

    @Contract(pure = true)
    public static int calcSpace(@NotNull Collection<Group> parties) {
        int space = 0;
        for (Group group : parties) {
            Optional<Integer> maxGroupSize = group.offer.maxGroupSize();
            if (maxGroupSize.isEmpty()) {
                return Integer.MAX_VALUE;
            }
            space += Math.max(0, maxGroupSize.get() - group.size());
        }
        return space;
    }

    // Begging ======================================================================================

    public void incrementBegging() {
        this.begging++;
    }

    public void deleteBegging() {
        this.begging = 0;
    }

    // ================================================================================================================

    public Offer getOffer() {
        return offer;
    }

    public int getMinGroupSize() {
        return offer.minGroupSize();
    }

    public int getMaxGroupSize() {
        return offer.maxGroupSize().orElse(Integer.MAX_VALUE);
    }

    public boolean fulfillsMinGroupSizeConstraint() {
        return getMinGroupSize() <= members.size();
    }

    // ================================================================================================================

    public void validate() {
        offer.validate();
        if (members.isEmpty()) throw new IllegalStateException();
        for (Member member : members) {
            member.validate(false);
        }
        if (begging < 0) throw new IllegalStateException();
        if (!fulfillsMinGroupSizeConstraint()) throw new IllegalStateException();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Offer: ").append(offer.toString()).append(", Demanders: ");
        for (Member member : members) {
            stringBuilder.append(member.toString()).append(", ");
        }
        return stringBuilder.toString();
    }
}
