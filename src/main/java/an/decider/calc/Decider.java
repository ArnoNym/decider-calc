package an.decider.calc;

import an.decider.Const;
import an.decider.input.Decision;
import an.decider.input.Demander;
import an.decider.input.Offer;
import an.decider.output.Choice;
import an.utils.SetUtils;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class Decider {
    public static String feedback = null;

    @NotNull
    public static List<Choice> createChoiceList(@NotNull Decision decision) throws IllegalArgumentException {
        decision.validate();

        if (decision.offers().isEmpty()) {
            feedback = "No offers!";
            return new ArrayList<>();
        }
        if (decision.demanders().isEmpty()) {
            feedback = "No demanders!";
            return new ArrayList<>();
        }

        Set<List<Offer>> offerPowerSet = SetUtils.createPowerSet(
                createAvailableOfferArrayList(decision.offers()),
                decision.minGroupCount(),
                decision.maxGroupCount().orElse(null));
        if (offerPowerSet.isEmpty()) {
            feedback = "Offer-Power-Set is Empty!";
            return new ArrayList<>();
        }

        List<Choice> choiceList = new LinkedList<>();
        for (List<Offer> offerList : offerPowerSet) {
            Set<Choice> choiceSet = Choice.createEqualChoices(decision.demanders(), offerList);
            if (choiceSet != null) {
                choiceList.addAll(choiceSet);
            }
        }
        if (choiceList.isEmpty()) {
            feedback = "ChoiceList is empty!";
            return new ArrayList<>();
        }

        choiceList.sort((o1, o2) -> (int) ((o2.calcUtility() - o1.calcUtility()) * Const.UTILITY_ORDER_DECIMALS));
        if (Const.DEVELOPER) {
            for (Choice choice : choiceList) {
                System.out.println(choice.toString() + "\n");
            }
        }

        return choiceList;
    }

    @NotNull
    public static ArrayList<Offer> createAvailableOfferArrayList(@NotNull Collection<? extends Offer> offers) {
        // TODO Min Group Count verstaerken. Summe and Gruppen mit den niedrigsten Anforderungen an Personen.
        // zb 3 mit 1 und 1 mit 3 bedeutet, dass fuer 6 Personen min 4 Gruppen
        ArrayList<Offer> availableOfferArrayList = new ArrayList<>();
        for (Offer offer : offers) {
            for (int i = 0; i < offer.available(); i++) {
                availableOfferArrayList.add(offer);
            }
        }
        return availableOfferArrayList;
    }
}
